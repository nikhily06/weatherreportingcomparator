package openweather;

import data.DataUtilities;
import io.restassured.response.Response;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.util.Map;

import static io.restassured.RestAssured.given;

public class OpenWeatherAPI {

    Response response;

    public int getResponseStatus() throws IOException, ParseException {
        DataUtilities dataUtilities = new DataUtilities();
        String location = dataUtilities.getLocation();

        APIConfigUtilities apiConfigUtilities = new APIConfigUtilities();
        String url = apiConfigUtilities.getUrl();
        String appID = apiConfigUtilities.getAppID();
        String units = apiConfigUtilities.getUnits();

        response = given()
                .queryParam("q", location)
                .queryParam("appid", appID)
                .queryParam("units", units)
                .when()
                .get(url);
        return response.statusCode();
    }

    public double getTemperature() {
        Map responseMap = response.as(Map.class);
        Map mainObj = (Map) responseMap.get("main");
        double temp = (double) mainObj.get("temp");
        return temp;
    }

}
