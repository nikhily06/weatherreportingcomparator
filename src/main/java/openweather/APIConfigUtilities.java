package openweather;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class APIConfigUtilities {

    Properties properties = new Properties();

    public APIConfigUtilities() throws IOException {
        String projectPath = System.getProperty("user.dir");
        FileInputStream file = new FileInputStream(projectPath + "/src/main/java/openweather/apiconfig.properties");
        properties.load(file);
    }

    public String getUrl() {
        return properties.getProperty("url");
    }

    public String getAppID() {
        return properties.getProperty("appid");
    }

    public String getUnits() {
        return properties.getProperty("units");
    }

}
