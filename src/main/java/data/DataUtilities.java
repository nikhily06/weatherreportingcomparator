package data;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileReader;
import java.io.IOException;

public class DataUtilities {

    JSONObject jsonObject;

    public DataUtilities() throws IOException, ParseException {
        String projectPath = System.getProperty("user.dir");
        JSONParser parser = new JSONParser();
        FileReader fileReader = new FileReader(projectPath + "/src/main/java/data/data.json");
        Object object = parser.parse(fileReader);
        jsonObject = (JSONObject) object;
    }

    public String getLocation() {
        return (String) jsonObject.get("location");
    }

    public double getTemperatureVariance() {
        return (double) jsonObject.get("temperatureVariance");
    }

}
