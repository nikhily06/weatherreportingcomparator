package accuWeatherUIPages;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class UIConfigUtilities {

    Properties properties = new Properties();

    public UIConfigUtilities() throws IOException {
        String projectPath = System.getProperty("user.dir");
        FileInputStream file = new FileInputStream(projectPath + "/src/main/java/accuWeatherUIPages/uiconfig.properties");
        properties.load(file);
    }

    public String getUrl() {
        return properties.getProperty("url");
    }

    public String getBrowser() {
        return properties.getProperty("browser");
    }

}
