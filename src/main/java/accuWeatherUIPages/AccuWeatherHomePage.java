package accuWeatherUIPages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class AccuWeatherHomePage extends PageInitialization {

    @FindBy(css = "input.search-input")
    WebElement searchLocationInput;

    public AccuWeatherHomePage(WebDriver driver) {
        super(driver);
    }

    public void enterSearchText(String location) {
        searchLocationInput.sendKeys(location + Keys.RETURN);
    }

}
