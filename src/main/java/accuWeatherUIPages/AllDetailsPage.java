package accuWeatherUIPages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class AllDetailsPage extends PageInitialization {

    @FindBy(css = "div.display-temp")
    WebElement tempDisplayDiv;

    public AllDetailsPage(WebDriver driver) {
        super(driver);
    }

    public double getTemperature() {
        String tempData = tempDisplayDiv.getText();
        double uiTemp = Double.parseDouble(tempData.substring(0, tempData.length() - 2));
        return uiTemp;
    }

}
