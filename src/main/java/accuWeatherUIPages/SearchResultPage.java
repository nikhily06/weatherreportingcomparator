package accuWeatherUIPages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SearchResultPage extends PageInitialization {

    @FindBy(css = "div.cur-con-weather-card__body")
    WebElement moreDetailsCard;

    @FindBy(id = "dismiss-button")
    WebElement dismissButton;

    public SearchResultPage(WebDriver driver) {
        super(driver);
    }

    public void clickMoreDetailsCard() {
        moreDetailsCard.click();
    }

    public void clickDismissButton() {
        dismissButton.click();
    }

}
