package openWeatherAPITests;

import openweather.OpenWeatherAPI;
import org.json.simple.parser.ParseException;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;

public class OpenWeatherAPITemperatureTests {

    OpenWeatherAPI openWeatherAPI = new OpenWeatherAPI();

    @Test(priority = 1)
    public void getResponseTest() throws IOException, ParseException {
        int statusCode = openWeatherAPI.getResponseStatus();
        Assert.assertTrue(statusCode == 200);
    }

    @Test(priority = 2)
    public void getTemperatureFromApi() {
        double temp = openWeatherAPI.getTemperature();
        System.out.println(temp);
    }

}

