package accuWeatherUITests;

import accuWeatherUIPages.AccuWeatherHomePage;
import accuWeatherUIPages.AllDetailsPage;
import accuWeatherUIPages.SearchResultPage;
import data.DataUtilities;
import org.json.simple.parser.ParseException;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;
import utilities.Utilities;

import java.io.IOException;

public class AccuWeatherUITemperatureTests {

    public static WebDriver driver;

    AccuWeatherHomePage accuWeatherHomePage;
    SearchResultPage searchResultPage;
    AllDetailsPage allDetailsPage;

    @BeforeSuite
    public void setup() throws IOException {
        driver = new Utilities().intializeDriver();
    }

    @Test(priority = 1)
    public void enterLocation() throws IOException, ParseException, InterruptedException {
        DataUtilities dataUtilities = new DataUtilities();

        accuWeatherHomePage = new AccuWeatherHomePage(driver);

        accuWeatherHomePage.enterSearchText(dataUtilities.getLocation());
    }

    @Test(priority = 2)
    public void clickMoreDetails() {
        searchResultPage = new SearchResultPage(driver);
        searchResultPage.clickMoreDetailsCard();
        searchResultPage.clickDismissButton();
    }

    @Test(priority = 3)
    public void getTemperatureFromPage() {
        allDetailsPage = new AllDetailsPage(driver);
        double temp = allDetailsPage.getTemperature();
        System.out.println(temp);
    }

    @AfterSuite
    public void tear() {
        driver.quit();
    }

}
