package comparatorTests;

import accuWeatherUIPages.AccuWeatherHomePage;
import accuWeatherUIPages.AllDetailsPage;
import accuWeatherUIPages.SearchResultPage;
import data.DataUtilities;
import openweather.OpenWeatherAPI;
import org.json.simple.parser.ParseException;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.Test;
import utilities.Utilities;

import java.io.IOException;

public class ComparatorTests {

    @Test
    public void compare() throws IOException, ParseException {

        WebDriver driver = new Utilities().intializeDriver();

        AccuWeatherHomePage accuWeatherHomePage = new AccuWeatherHomePage(driver);
        SearchResultPage searchResultPage = new SearchResultPage(driver);
        AllDetailsPage allDetailsPage = new AllDetailsPage(driver);

        DataUtilities dataUtilities = new DataUtilities();

        accuWeatherHomePage.enterSearchText(dataUtilities.getLocation());

        searchResultPage.clickMoreDetailsCard();

        double uiTemp = allDetailsPage.getTemperature();
        System.out.println(uiTemp);

        driver.quit();

        OpenWeatherAPI openWeatherAPI = new OpenWeatherAPI();

        int statusCode = openWeatherAPI.getResponseStatus();
        double apiTemp = openWeatherAPI.getTemperature();
        System.out.println(apiTemp);

        Assert.assertTrue((statusCode == 200) && (Math.abs(uiTemp - apiTemp) < dataUtilities.getTemperatureVariance()));

        if (statusCode != 200) {
            System.out.println("Status Code Error");
        } else if (Math.abs(uiTemp - apiTemp) > dataUtilities.getTemperatureVariance()) {
            System.out.println("Difference in Temperature more than variance");
        }
    }

}
